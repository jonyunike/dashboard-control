/**
 * Theme: Unike Control Admin Dashboard
 * Dashboard Unike Control
 * am4Charts
 */

am4core.ready(function() {
            
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var iconPath = "M65.886 22.972A22.471 22.471 0 1143.415.5a22.471 22.471 0 0122.471 22.472z M60.33 51.057H25.391A24.891 24.891 0 00.5 75.948v35.707a24.872 24.872 0 0012.34 21.493 1.974 1.974 0 002.98-1.69v-53.3a1.562 1.562 0 011.562-1.562h0a1.562 1.562 0 011.562 1.562v126.806c0 6.162 4.748 11.535 10.906 11.719a11.253 11.253 0 0011.6-11.247v-64.341a1.562 1.562 0 011.562-1.562h0a1.562 1.562 0 011.562 1.562v63.869c0 6.162 4.748 11.535 10.907 11.719a11.252 11.252 0 0011.6-11.247V78.157a1.562 1.562 0 011.562-1.562h0a1.562 1.562 0 011.562 1.562v53.117a1.973 1.973 0 003 1.679 24.863 24.863 0 0012.019-21.3V75.948A24.892 24.892 0 0060.33 51.057z"
    // var iconPath = "M53.5,476c0,14,6.833,21,20.5,21s20.5-7,20.5-21V287h21v189c0,14,6.834,21,20.5,21 c13.667,0,20.5-7,20.5-21V154h10v116c0,7.334,2.5,12.667,7.5,16s10.167,3.333,15.5,0s8-8.667,8-16V145c0-13.334-4.5-23.667-13.5-31 s-21.5-11-37.5-11h-82c-15.333,0-27.833,3.333-37.5,10s-14.5,17-14.5,31v133c0,6,2.667,10.333,8,13s10.5,2.667,15.5,0s7.5-7,7.5-13 V154h10V476 M61.5,42.5c0,11.667,4.167,21.667,12.5,30S92.333,85,104,85s21.667-4.167,30-12.5S146.5,54,146.5,42 c0-11.335-4.167-21.168-12.5-29.5C125.667,4.167,115.667,0,104,0S82.333,4.167,74,12.5S61.5,30.833,61.5,42.5z"
    
    var chart = am4core.create("chartdiv", am4charts.SlicedChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
    
    chart.data = [{
        "name": "Lorem 1",
        "value": 254
    }, {
        "name": "Lorem 2",
        "value": 300
    }];
    
    var series = chart.series.push(new am4charts.PictorialStackedSeries());
    series.dataFields.value = "value";
    series.dataFields.category = "name";
    series.alignLabels = false;

    // series.legendSettings.labelText = "Series: [bold {color}]{name}[/]";
    
    series.maskSprite.path = iconPath;

    series.ticks.template.locationX = 0.5;
    series.ticks.template.locationY = 0.5;

    series.labelsContainer.width = 100;

    series.colors.list = [
        am4core.color("rgba(0, 188, 212, 0.85)")
    ];
    
    chart.legend = new am4charts.Legend();
    chart.legend.position = "right";
    chart.legend.valign = "top";
    chart.legend.labels.template.fill = am4core.color("#fff");
    chart.legend.valueLabels.template.fill = am4core.color("#fff");

    // WOMAN
    var iconPathWoman = "M70.196 22.972A22.471 22.471 0 1147.725.5a22.471 22.471 0 0122.471 22.472z M94.46 163.456l-12.4-31.737a25.232 25.232 0 008.693-19.042V76.299a25.36 25.36 0 00-25.36-25.36h-35.6a25.36 25.36 0 00-25.36 25.36v36.379a25.236 25.236 0 008.694 19.04l-12.4 31.738h22.5v44.284c0 6.278 4.837 11.752 11.112 11.94a11.464 11.464 0 0011.817-11.459v-44.765h3.183v44.284c0 6.278 4.838 11.752 11.112 11.94a11.464 11.464 0 0011.817-11.459v-44.765zm-20.6-86.5h0a1.592 1.592 0 011.592 1.592v36.256l-3.183-8.148V78.549a1.592 1.592 0 011.593-1.591zm-53.814 37.059V78.549a1.592 1.592 0 113.183 0v27.32z"

    var chart1 = am4core.create("chartdiv1", am4charts.SlicedChart);
    chart1.hiddenState.properties.opacity = 0; // this makes initial fade in effect
    
    chart1.data = [{
        "name": "Lorem 1",
        "value": 100
    }, {
        "name": "Lorem 2",
        "value": 300
    }];
    
    var series1 = chart1.series.push(new am4charts.PictorialStackedSeries());
    series1.dataFields.value = "value";
    series1.dataFields.category = "name";
    series1.alignLabels = false;

    // series.legendSettings.labelText = "Series: [bold {color}]{name}[/]";
    
    series1.maskSprite.path = iconPathWoman;

    series1.ticks.template.locationX = 0.5;
    series1.ticks.template.locationY = 0.5;

    series1.labelsContainer.width = 100;

    series1.colors.list = [
        am4core.color("rgba(236, 64, 143, 0.85)"),
        am4core.color("rgba(185, 31, 137, 0.85)")
    ];
    
    chart1.legend = new am4charts.Legend();
    chart1.legend.position = "right";
    chart1.legend.valign = "top";
    chart1.legend.labels.template.fill = am4core.color("#fff");
    chart1.legend.valueLabels.template.fill = am4core.color("#fff");
});