/**
 * Theme: Unike Control Admin Dashboard
 * Dashboard Unike Control
 * Responsive-table Js
 */
 
$(function() {
  $('.table-responsive').responsiveTable({
      addDisplayAllBtn: 'btn btn-secondary'
  });
});