/**
 * Theme: Unike Control Admin Dashboard
 * Dashboard Unike Control
 * Tooltip Js
 */

(function ($) {

    "use strict";

    tippy('.tippy-btn');       
    tippy('#myElement', {
        html: document.querySelector('#feature__html'), // DIRECT ELEMENT option
        arrow: true,
        animation: 'fade'
    });

})(jQuery);